﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;

namespace KobenStaffDirectoryRKing.Models
{
    public class StaffDetail
    {
        // This class is the POCO
        public int Id { get; set; }
        [StringLength(30)]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }
        [StringLength(40)]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }
        [StringLength(20)]
        public string Title { get; set; }
        public int Phone { get; set; }
        [Display(Name = "Photo")]
        public byte[] StaffPhoto { get; set; }
		public string test2 {get; set;}
        
        
    }
}
