﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KobenStaffDirectoryRKing.Models;
using KobenStaffDirectoryRKing.ViewModels;

namespace KobenStaffDirectoryRKing.Data
{
    public interface IStaffDirectoryRepository : IDisposable
    {
        Task<IEnumerable<StaffDetailViewModel>> GetAllStaffDetails();
        Task<StaffDetailViewModel> GetStaffDetailById(int? id);
        Task CreateStaffDetail(StaffDetailViewModel staffDetail);
        void UpdateStaffDetail(StaffDetailViewModel staffDetail);
        void Save();
    }
}
