﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using KobenStaffDirectoryRKing.ViewModels;
using System.Threading;
using System.Threading.Tasks;
using KobenStaffDirectoryRKing.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Http;

namespace KobenStaffDirectoryRKing.Data
{
    public class StaffDirectoryRepository : IStaffDirectoryRepository, IDisposable
    {
        private readonly StaffDirectoryContext _context;
        private bool disposed = false;

        public StaffDirectoryRepository(StaffDirectoryContext staffDirectoryContext)
        {
            _context = staffDirectoryContext;
        }
        

        public async Task<IEnumerable<StaffDetailViewModel>> GetAllStaffDetails()
        {
            var staffList = await _context.StaffDetails.ToListAsync();
            return CreateViewModelObject(staffList);
        }

        public async Task<StaffDetailViewModel> GetStaffDetailById(int? id)
        {
            var staff = await _context.StaffDetails.FindAsync(id);

            return CreateViewModelObject(new []{staff}).FirstOrDefault();

        }

        public async Task CreateStaffDetail(StaffDetailViewModel staffDetail)
        {
            var staff = CreateStaffDetailObject(staffDetail);
           await _context.StaffDetails.AddAsync(staff);
            Save();
        }

        

        public void UpdateStaffDetail(StaffDetailViewModel staffDetail)
        {
            var staff = CreateStaffDetailObject(staffDetail);
             _context.StaffDetails.Update(staff);
            Save();
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (this.disposed) return;
            if (disposing)
            {
                _context.Dispose();
            }
            this.disposed = true;
        }

        private static StaffDetail CreateStaffDetailObject(StaffDetailViewModel staffDetail)
        {
            return new StaffDetail
            {
                FirstName = staffDetail.FirstName,
                LastName = staffDetail.LastName,
                Title = staffDetail.Title,
                Phone = staffDetail.Phone,
                StaffPhoto = ConvertIFormFileToByteArray(staffDetail.StaffPhotoFile)
            };
        }

        private static IEnumerable<StaffDetailViewModel> CreateViewModelObject(IEnumerable<StaffDetail> staffDetails)
        {
            return staffDetails.Select(staff => new StaffDetailViewModel
                {
                    FirstName = staff.FirstName,
                    LastName = staff.LastName,
                    Id = staff.Id,
                    Phone = staff.Phone,
                    PhotoUrl = ConvertByteArrayToStringUrlForImage(staff.StaffPhoto),
                    Title = staff.Title
                })
                .ToList();
        }
        private static byte[] ConvertIFormFileToByteArray(IFormFile staffPhotoFile)
        {

            using (var memstream = new MemoryStream())
            {
                staffPhotoFile.OpenReadStream().CopyTo(memstream);
                return memstream.ToArray();
            }
                

        }

        private static string ConvertByteArrayToStringUrlForImage(byte[] array)
        {
           
                var base64String = Convert.ToBase64String(array);
                return $"data:image/png;base64 {base64String}";
                
        }
        
    }
}