﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KobenStaffDirectoryRKing.Models;

namespace KobenStaffDirectoryRKing.Data
{
    public class DbInitialiser
    {
        public static void Initialise(StaffDirectoryContext context)
        {
            // This method checks to see if the DB has 
            // been created and creates if it needs to be.
            context.Database.EnsureCreated();
        }
    }
}
