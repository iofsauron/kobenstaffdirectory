﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KobenStaffDirectoryRKing.Models;
using Microsoft.EntityFrameworkCore;

namespace KobenStaffDirectoryRKing.Data
{
    public class StaffDirectoryContext : DbContext
    {
        public StaffDirectoryContext(DbContextOptions<StaffDirectoryContext> options) : base(options)
        {
            
        }
        // This class sets up the Context for all DB operations
        public DbSet<StaffDetail> StaffDetails { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<StaffDetail>().ToTable("Staff");
        }
    }
}
