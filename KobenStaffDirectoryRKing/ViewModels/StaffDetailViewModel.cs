﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace KobenStaffDirectoryRKing.ViewModels
{
    public class StaffDetailViewModel
    {
        public int Id { get; set; }
        [StringLength(30)]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }
        [StringLength(40)]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }
        [StringLength(20)]
        public string Title { get; set; }
        public int Phone { get; set; }
        [Display(Name = "Photo Upload")]
        public IFormFile StaffPhotoFile { get; set; }
        public string PhotoUrl { get; set; }
        

    }
}
