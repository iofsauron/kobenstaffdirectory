﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KobenStaffDirectoryRKing.Data;
using KobenStaffDirectoryRKing.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;


namespace KobenStaffDirectoryRKing
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Here I am adding the DI for the context class
            services.AddEntityFrameworkSqlServer()
                .AddDbContext<StaffDirectoryContext>(
                a => a.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));
            // Here I'm adding Dependancy Injection
            services.AddScoped<IStaffDirectoryRepository, StaffDirectoryRepository>();
            // Add framework services.
            services.AddMvc();
            
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory, StaffDirectoryContext context)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseBrowserLink();
            }
            else
            {
                //Changing the default error page
                app.UseExceptionHandler("/StaffDetails/Error");
            }

            app.UseStaticFiles();

            app.UseMvc(routes =>
            {
                //routes.MapRoute(
                //    name: "default",
                //    template: "{controller=Home}/{action=Index}/{id?}");

                // modifying the routes so that the default is StaffDirectory
                routes.MapRoute(name: "default",
                    template: "{controller=StaffDetails}/{action=Index}/{id?}");
            });
            // This call here will check if the database needs
            // to be created
            DbInitialiser.Initialise(context);
        }
    }
}
